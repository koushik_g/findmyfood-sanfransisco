FindMyFood App
--------------------------------
In order to use the app you need to follow the below two steps-
1. You need to have a Google Maps API Key. Please refer this page on how to generate an API Key for Google Maps - https://developers.google.com/maps/documentation/javascript/get-api-key
2. You need to have a app token for consuming the San Fransico's mobile food schedule public API. For that you can go to this link - https://dev.socrata.com/foundry/data.sfgov.org/jjew-r69b
    - There go to the app token section and sign up for an account and in there you have options to generate API Keys and App Tokens.
    - We need to generate App Token.1

Once you complete the above two steps and have the keys ready -
- Go to local.properties file and add the keys in their respective fields

Architecture:
-------------
The architecture of this app is based on -
*) MVVM pattern(to totally separate out business logic from the View layer and increase the testability of code),
*) used dagger-Hilt for dependency injection (for decoupling and increasing the re-usability of code,
   usage of Hilt library for dependency injection will also give us the control to define the lifecycle
   scope of the dependencies),
*) coroutines for async calls,
*) Used Retrofit2 HTTP client for making API calls.
