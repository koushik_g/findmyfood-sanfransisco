package com.example.findmyfood.di

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class FindMyFoodApplication : Application()