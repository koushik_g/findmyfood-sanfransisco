package com.example.findmyfood.main.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.findmyfood.common.Constants
import com.example.findmyfood.common.DispatcherProvider
import com.example.findmyfood.common.Resource
import com.example.findmyfood.data.models.Truck
import com.example.findmyfood.main.repository.MainRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.time.LocalDate
import java.time.LocalTime
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val repository: MainRepository,
    private val dispatchers: DispatcherProvider,
    private val isNetworkConnected: Boolean
) : ViewModel() {

    sealed class FoodTruckListEvent {
        class Success(val resultList: List<Truck>) : FoodTruckListEvent()
        class Failure(val errorText: String) : FoodTruckListEvent()
        object Loading : FoodTruckListEvent()
        object Empty : FoodTruckListEvent()
    }

    private val mTrucksList = MutableLiveData<FoodTruckListEvent>(FoodTruckListEvent.Empty)
    val trucksList: LiveData<FoodTruckListEvent> = mTrucksList

    fun loadTrucksList() {
        //handling network failure
        if (!isNetworkConnected) {
            mTrucksList.value = FoodTruckListEvent.Failure(Constants.INTERNET_ERROR)
            return
        }

        viewModelScope.launch(dispatchers.io) {
            mTrucksList.postValue(FoodTruckListEvent.Loading)
            val day = LocalDate.now().dayOfWeek.value
            when (val truckListResponse =
                repository.getFoodTrucksList(if (day == Constants.SEVEN) Constants.ZERO else day)) {
                is Resource.Error -> mTrucksList.postValue(
                    FoodTruckListEvent.Failure(
                        truckListResponse.message.toString()
                    )
                )
                is Resource.Success -> {
                    mTrucksList.postValue(
                        FoodTruckListEvent.Success(
                            getOpenTrucksList(
                                truckListResponse.data!!
                            )
                        )
                    )
                }
            }
        }
    }

    private fun getOpenTrucksList(oldList: List<Truck>): List<Truck> {
        val openTrucksList = mutableListOf<Truck>()
        val currentTime = LocalTime.now()
        var modifiedEnd: String
        for (i in oldList) {
            modifiedEnd = i.end24
            if (modifiedEnd == Constants.DEFAULT_INVALID_TIME) {
                modifiedEnd = Constants.MODIFIED_TIME
            }
            if (currentTime >= LocalTime.parse(i.start24) && currentTime <= LocalTime.parse(
                    modifiedEnd
                )
            ) {
                openTrucksList.add(i)
            }
        }
        return openTrucksList
    }
}