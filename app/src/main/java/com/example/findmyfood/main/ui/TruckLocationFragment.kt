package com.example.findmyfood.main.ui

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.example.findmyfood.common.CommonUtils
import com.example.findmyfood.data.models.Truck
import com.example.findmyfood.main.viewmodel.MainViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TruckLocationFragment : SupportMapFragment(), OnMapReadyCallback,
    GoogleMap.OnMarkerClickListener {
    private lateinit var viewModel: MainViewModel
    private lateinit var trucksList: List<Truck>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val activity = activity as MainActivity
        viewModel = activity.viewModel

        viewModel.trucksList.observe(viewLifecycleOwner, Observer {
            when (it) {
                is MainViewModel.FoodTruckListEvent.Success -> {
                    trucksList = it.resultList
                    getMapAsync(this)
                }
                is MainViewModel.FoodTruckListEvent.Failure -> {
                    CommonUtils.showOneButtonDialog(activity, it.errorText)
                }
                else -> Unit
            }
        })
    }

    override fun onMapReady(map: GoogleMap) {
        if (null != trucksList && trucksList.isNotEmpty()) {
            lateinit var loc: LatLng
            val boundBuilder = LatLngBounds.Builder()
            lateinit var marker: Marker
            trucksList.forEach { item ->
                loc = LatLng(item.latitude.toDouble(), item.longitude.toDouble())
                marker = map.addMarker(MarkerOptions().position(loc))!!
                marker.tag = item
                boundBuilder.include(loc)
            }
            val finalBounds = boundBuilder.build()

            map.animateCamera(CameraUpdateFactory.newLatLngBounds(finalBounds, 20))
            map.setOnMarkerClickListener(this)
        }
    }

    override fun onMarkerClick(marker: Marker): Boolean {
        val bottomSheet = TruckBottomSheetDialog.newInstance(marker.tag as Truck)
        activity?.supportFragmentManager?.let { bottomSheet.show(it, "BottomSheet") }
        return false
    }

}