package com.example.findmyfood.main.repository

import com.example.findmyfood.common.Constants
import com.example.findmyfood.common.Resource
import com.example.findmyfood.data.models.Truck
import com.example.findmyfood.data.service.FoodTruckAPI
import java.lang.Exception
import javax.inject.Inject

class DefaultRepository @Inject constructor(
    private val api: FoodTruckAPI
) : MainRepository {

    override suspend fun getFoodTrucksList(dayOrder: Int): Resource<List<Truck>> {
        return try {
            val response = api.getAvailableTrucksList(String.format(Constants.CONDITION, dayOrder))
            lateinit var result: List<Truck>

            if (null != response && response.code() == 200) {
                result = response.body()!!
            } else {
                return Resource.Error(Constants.SERVICE_ERROR)
            }

            if (response.isSuccessful && !result.isNullOrEmpty()) {
                Resource.Success(result)
            } else {
                return Resource.Error(Constants.SERVICE_ERROR)
            }
        } catch (ex: Exception) {
            Resource.Error(Constants.PARSING_ERROR)
        }
    }

}