package com.example.findmyfood.main.ui

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.findmyfood.R
import com.example.findmyfood.common.CommonUtils
import com.example.findmyfood.databinding.FragmentTruckListBinding
import com.example.findmyfood.main.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TruckListFragment : Fragment(R.layout.fragment_truck_list) {
    private lateinit var binding: FragmentTruckListBinding
    private lateinit var viewModel: MainViewModel
    private lateinit var truckAdapter: TruckListAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val activity = activity as MainActivity
        binding = FragmentTruckListBinding.bind(view)
        viewModel = activity.viewModel
        truckAdapter = TruckListAdapter(activity)

        binding.truckRecycler.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = truckAdapter
            addItemDecoration(DividerItemDecoration(this.context, LinearLayoutManager.VERTICAL))
        }

        viewModel.trucksList.observe(viewLifecycleOwner, Observer {
            when (it) {
                is MainViewModel.FoodTruckListEvent.Success -> {
                    binding.listFragProgress.isVisible = false
                    truckAdapter.truckList = it.resultList
                }
                is MainViewModel.FoodTruckListEvent.Failure -> {
                    binding.listFragProgress.isVisible = false
                    CommonUtils.showOneButtonDialog(activity, it.errorText)
                }
                is MainViewModel.FoodTruckListEvent.Loading -> {
                    binding.listFragProgress.isVisible = true
                }
                else -> Unit
            }
        })
    }
}