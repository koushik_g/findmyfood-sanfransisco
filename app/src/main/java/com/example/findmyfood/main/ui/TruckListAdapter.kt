package com.example.findmyfood.main.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.findmyfood.R
import com.example.findmyfood.data.models.Truck
import com.example.findmyfood.databinding.RowItemBinding

class TruckListAdapter(private val context: Context) :
    RecyclerView.Adapter<TruckListAdapter.TruckViewHolder>() {

    inner class TruckViewHolder(val itemBinding: RowItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root)

    private val diffCallBack = object : DiffUtil.ItemCallback<Truck>() {
        override fun areItemsTheSame(oldItem: Truck, newItem: Truck): Boolean {
            return oldItem.cnn == newItem.cnn
        }

        override fun areContentsTheSame(oldItem: Truck, newItem: Truck): Boolean {
            return oldItem == newItem
        }
    }

    private val differ = AsyncListDiffer(this, diffCallBack)

    var truckList: List<Truck>
        get() = differ.currentList
        set(value) {
            differ.submitList(value)
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TruckViewHolder {
        return TruckViewHolder(
            RowItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: TruckViewHolder, position: Int) {
        val item = truckList[position]
        if (null != item) {
            holder.itemBinding.apply {
                truckName.text = item.applicant
                truckLocation.text = item.location
                description.text = item.optionaltext
                truckTime.text = String.format(
                    context.getString(R.string.truckTimings),
                    item.starttime,
                    item.endtime
                )
            }
        }
    }

    override fun getItemCount(): Int {
        return truckList.size
    }

}