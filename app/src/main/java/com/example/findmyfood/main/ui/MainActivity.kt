package com.example.findmyfood.main.ui

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.findmyfood.R
import com.example.findmyfood.common.Constants
import com.example.findmyfood.databinding.ActivityMainBinding
import com.example.findmyfood.main.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    val viewModel: MainViewModel by viewModels()
    private lateinit var fragment1: TruckListFragment
    private lateinit var fragment2: TruckLocationFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        TimeZone.setDefault(TimeZone.getTimeZone(Constants.PST))
        viewModel.loadTrucksList()

        fragment1 = TruckListFragment()
        fragment2 = TruckLocationFragment()

        setCurrentFragment(fragment1)

        binding.bottomNav.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.listMode -> setCurrentFragment(fragment1)
                R.id.mapMode -> setCurrentFragment(fragment2)
            }
            true
        }
    }

    private fun setCurrentFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.root_fl, fragment)
            if (fragment is TruckLocationFragment) {
                addToBackStack(null)
            }
            commit()
        }
    }

    override fun onBackPressed() {
        if (fragment1.isVisible) {
            finish()
        } else {
            super.onBackPressed()
        }
    }

}