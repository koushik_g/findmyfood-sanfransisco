package com.example.findmyfood.main.repository

import com.example.findmyfood.common.Resource
import com.example.findmyfood.data.models.Truck

interface MainRepository {

    suspend fun getFoodTrucksList(
        dayOrder: Int
    ): Resource<List<Truck>>

}