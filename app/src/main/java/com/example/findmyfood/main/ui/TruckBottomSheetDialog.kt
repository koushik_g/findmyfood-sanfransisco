package com.example.findmyfood.main.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.findmyfood.R
import com.example.findmyfood.data.models.Truck
import com.example.findmyfood.databinding.RowItemBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class TruckBottomSheetDialog : BottomSheetDialogFragment() {
    private lateinit var binding: RowItemBinding

    companion object {
        private const val TRUCKMODEL = "model"

        fun newInstance(model: Truck): TruckBottomSheetDialog {
            val args = Bundle()
            args.putParcelable(TRUCKMODEL, model)
            val fragment = TruckBottomSheetDialog()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = RowItemBinding.inflate(inflater, container, false)
        val model = requireArguments().getParcelable<Truck>(TRUCKMODEL) as Truck

        binding.truckName.text = model.applicant
        binding.truckLocation.text = model.location
        binding.description.text = model.optionaltext
        binding.truckTime.text = activity?.getString(R.string.truckTimings)
            ?.let { String.format(it, model.starttime, model.endtime) }

        return binding.root
    }

}