package com.example.findmyfood.data.service

import com.example.findmyfood.BuildConfig
import com.example.findmyfood.common.Constants
import com.example.findmyfood.data.models.Truck
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface FoodTruckAPI {

    @GET("/resource/jjew-r69b.json")
    suspend fun getAvailableTrucksList(
        @Query("\$where") condition: String,
        @Query("\$order") order: String = Constants.APPLICANT,
        @Query("\$\$app_token") appToken: String = BuildConfig.api_app_token
    ): Response<List<Truck>>

}