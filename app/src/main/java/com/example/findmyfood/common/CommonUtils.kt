package com.example.findmyfood.common

import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog

object CommonUtils {

    fun showOneButtonDialog(context: Context, message: String) {
        val builder = AlertDialog.Builder(context)
        builder.setMessage(message)
            .setCancelable(false)
            .setPositiveButton(android.R.string.ok, DialogInterface.OnClickListener { dialog, _ ->
                dialog.cancel()
            })
        val alert: AlertDialog = builder.create()
        alert.show()
    }

}