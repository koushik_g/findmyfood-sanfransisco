package com.example.findmyfood.common

object Constants {
    const val BASE_URL = "https://data.sfgov.org/"
    const val SERVICE_ERROR = "Service call Failed!"
    const val PARSING_ERROR = "Response Parsing failed!"
    const val CONDITION = "dayorder=%d"
    const val INTERNET_ERROR = "No Internet Connection"
    const val PST = "America/Los_Angeles"
    const val DEFAULT_INVALID_TIME = "24:00"
    const val MODIFIED_TIME = "23:59:59"
    const val APPLICANT = "applicant"
    const val SEVEN = 7
    const val ZERO = 0
}